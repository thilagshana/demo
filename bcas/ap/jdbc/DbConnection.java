package bcas.ap.jdbc;

import java.beans.Statement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DbConnection {
	static final String JDBC_DRIVER = "com.mysql.cj.jdbc.Driver";
	static final String DB_URL = "jdbc:mysql://Localhost:3306/bcas";
	static final String USER = "root";
	static final String PASS = "root";

	public static void main(String[] args) {
		Connection conn = null;
		Statement statement = null;

		try {
			Class.forName(JDBC_DRIVER);
			System.out.println("connection to database");
			conn = DriverManager.getConnection(DB_URL, USER, PASS);
			//conn.close();
			System.out.println("connection Success");

		} catch (SQLException se) {
			se.printStackTrace();

		} catch (ClassNotFoundException e) {
			e.printStackTrace();

		} finally {
			// finally llack used to close resources
			try {
				if (statement != null)
					((Connection) statement).close();

			} catch (SQLException se) {
			}
			try {
				if (conn != null)
					conn.close();

			} catch (SQLException se) {
				se.printStackTrace();

			}
			System.out.println("Good Bye");
		}

	}
}