package bcas.ap.invoi;

public class Invoice {
	String PartNo;
	String PartDescription;
	int ItemQuantity;
	double ItemPrice;

	public Invoice(String partNo, String partDescription, int itemQuantity, double itemPrice) {
		super();
		PartNo = partNo;
		PartDescription = partDescription;
		ItemQuantity = itemQuantity;
		ItemPrice = itemPrice;
	}

	public String getPartNo() {
		return PartNo;
	}

	public void setPartNo(String partNo) {
		PartNo = partNo;
	}

	public String getPartDescription() {
		return PartDescription;
	}

	public void setPartDescription(String partDescription) {
		PartDescription = partDescription;
	}

	public int getItemQuantity() {
		return ItemQuantity;
	}

	public void setItemQuantity(int itemQuantity) {
		ItemQuantity = itemQuantity;
	}

	public double getItemPrice() {
		return ItemPrice;
	}

	public void setItemPrice(double itemPrice) {
		ItemPrice = itemPrice;
	}

	public double getInvoice_amount() {
		double total;
		total = ItemPrice * ItemQuantity;
		return total;
	}
}
