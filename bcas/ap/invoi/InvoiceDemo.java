package bcas.ap.invoi;

import java.util.Scanner;

public class InvoiceDemo {

	public static void main(String[] args) {

		String partNo = null;
		String partDescription = null;
		int itemQuantity = 0;
		double itemPrice = 0;

		Invoice invoice = new Invoice(partNo, partDescription, itemQuantity, itemPrice);

		Scanner scan = new Scanner(System.in);

		System.out.println("Enter part no- ");
		partNo = scan.nextLine();
		invoice.setPartNo(partNo);

		System.out.println("Enter partDescription- ");
		partDescription = scan.nextLine();
		invoice.setPartDescription(partDescription);

		System.out.println("Enter itemQuantity- ");
		itemQuantity = scan.nextInt();
		invoice.setItemQuantity(itemQuantity);

		System.out.println("Enter itemPrice- ");

		itemPrice = scan.nextDouble();
		invoice.setItemPrice(itemPrice);
		
	
	}
}
