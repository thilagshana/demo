package bcas.edu.excep;

public class NullException {
	public static void main(String[] args) {

		String str1 = "THILAGSHANA";
		System.out.println(str1.length());

		try {
			String str2 = null;
			System.out.println(str2.length());
		} catch (NullPointerException e) {
			System.out.println("String is null");
		}
		String str3 = "BCAS";
		System.out.println(str3.length());

	}
}
